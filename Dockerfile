FROM registry.dune-project.org/staging/dune-python:unstable
MAINTAINER "Martin Nolte"

USER root

RUN apt-get update && apt-get dist-upgrade --yes --no-install-recommends
RUN apt-get install --yes --no-install-recommends \
  libeigen3-dev libsuitesparse-dev \
  python3-ufl

ADD install-twophasedg.sh /root/
RUN /root/install-twophasedg.sh

# COPY twophaseflow.ipynb limit.py utility.hh /dune/
# RUN chown dune:dune /dune/twophaseflow.ipynb /dune/limit.py /dune/utility.hh

USER dune

RUN cp -r /usr/local/share/doc/dune-fempy/jupyter /dune/dune-fempy
COPY --chown=dune:dune twophaseflow.ipynb limit.py utility.hh /dune/
